import { createFighters } from './components/fightersView';
import { fighterService } from './services/fightersService';

class App {
  private rootElement:HTMLElement= this.htmlNullChecker(document.getElementById('root'));
  private loadingElement:HTMLElement= this.htmlNullChecker(document.getElementById('loading-overlay'));

  constructor() {
    this.startApp();
  }

  private htmlNullChecker(el:HTMLElement |null): HTMLElement {
    if (el === null) {
      throw new Error("Can't access to element!")
    }
    return el;
  }

  private async startApp():Promise<void> {
    try {
      this.loadingElement.style.visibility = 'visible';

      const fighters = await fighterService.getFighters();
      const fightersElement = createFighters(fighters);

      this.rootElement.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      this.rootElement.innerText = 'Failed to load data';
    } finally {
      this.loadingElement.style.visibility = 'hidden';
    }
  }
}

export default App;
