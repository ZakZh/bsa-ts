import { controls } from '../../constants/controls';
import {fullFighterInt, fighterInFightInt} from '../interfaces/fightersInterfaces'

export async function fight(firstFighter:fullFighterInt, secondFighter:fullFighterInt) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over

    let firstPlayerPosition:string = 'left';
    let secondPlayerPosition:string = 'right';

    let firstPlayerStatus = {
      ...firstFighter,
      firstPlayerPosition,
      blockStatus: false,
      healthBar: getPlayerHealthBar(firstPlayerPosition),
      healthOnStart: firstFighter.health,
      fighterCombos: new Set(controls.PlayerOneCriticalHitCombination),
      comboStatus: {
        lastCombo: Date,
        comboInput: new Set(),
      },
    };

    let secondPlayerStatus = {
      ...secondFighter,
      secondPlayerPosition,
      blockStatus: false,
      healthBar: getPlayerHealthBar(secondPlayerPosition),
      healthOnStart: secondFighter.health,
      fighterCombos: new Set(controls.PlayerTwoCriticalHitCombination),
      comboStatus: {
        lastCombo: Date,
        comboInput: new Set(),
      },
    };

    function getPlayerHealthBar(position:string) {
      let fighterHealthBar = document.getElementById(`${position}-fighter-indicator`);
      return fighterHealthBar;
    }

    document.addEventListener('keydown', playerAction);
    document.addEventListener('keyup', playerAction);

    function makeWinner(winner:fighterInFightInt) {
      document.removeEventListener('keydown', playerAction);
      document.removeEventListener('keyup', playerAction);
      resolve(winner);
    }

    function strikeDamage(defender:fighterInFightInt, damage:number) {
      defender.health = defender.health - damage;
      defender.healthBar.style.width = (100 * defender.health) / defender.healthOnStart + '%';

      if (defender.health <= 0) {
        defender.healthBar.style.width = "0";
        return true;
      }

      return false;
    }

    function makeHit(attacker:fighterInFightInt, defender:fighterInFightInt, eventType:string):void {
      if (eventType == 'keydown' && !attacker.blockStatus && !defender.blockStatus) {
        let damage = getDamage(attacker, defender);
        if (strikeDamage(defender, damage)) {
          makeWinner(attacker);
        }
      }
    }

    function makeComboHit(attacker:fighterInFightInt, defender:fighterInFightInt, eventType:string):void {
      if (eventType == 'keydown' && !attacker.blockStatus) {
        let damage = getComboCriticalDamage(attacker);
        if (strikeDamage(defender, damage)) {
          makeWinner(attacker);
        }
      }
    }

    function getComboCriticalDamage(fighter:fighterInFightInt):number {
      //return Combo power
      let attack = fighter.attack;
      let power = attack * 2;
      return power;
    }

    function toggleBlock(fighter:fighterInFightInt):void {
      fighter.blockStatus = !fighter.blockStatus;
    }

    function checkComboStatus(fighter:fighterInFightInt, inputCode:string, inputType:string):boolean {
      let currentTime:number = Date.now();

      if (currentTime - Number(fighter.comboStatus.lastCombo) < 10000) {
        return false;
      }

      if (inputType == 'keyup' && fighter.fighterCombos.has(inputCode)) {
        fighter.comboStatus.comboInput.clear();
        return false;
      }

      if (inputType == 'keydown' && fighter.fighterCombos.has(inputCode)) {
        fighter.comboStatus.comboInput.add(inputCode);
      } else {
        return false;
      }

      if (fighter.blockStatus === true) {
        return false;
      }

      for (let code of fighter.fighterCombos) {
        if (!fighter.comboStatus.comboInput.has(code)) {
          return false;
        }
      }

      fighter.comboStatus.comboInput.clear();
      fighter.comboStatus.lastCombo = Date.now();
      return true;
    }

    function playerAction(event) {
      if (!event.repeat) {
        let eventCode = event.code;
        let eventType = event.type;

        switch (eventCode) {
          case controls.PlayerOneAttack: {
            makeHit(firstPlayerStatus, secondPlayerStatus, eventType);
            break;
          }
          case controls.PlayerOneBlock: {
            toggleBlock(firstPlayerStatus);
            break;
          }
          case controls.PlayerTwoAttack: {
            makeHit(secondPlayerStatus, firstPlayerStatus, eventType);
            break;
          }
          case controls.PlayerTwoBlock: {
            toggleBlock(secondPlayerStatus);
            break;
          }
        }

        if (checkComboStatus(firstPlayerStatus, eventCode, eventType)) {
          makeComboHit(firstPlayerStatus, secondPlayerStatus, eventType);
        }

        if (checkComboStatus(secondPlayerStatus, eventCode, eventType)) {
          makeComboHit(secondPlayerStatus, firstPlayerStatus, eventType);
        }
      }
    }
  });
}

export function getDamage(attacker:fighterInFightInt, defender:fighterInFightInt):number {
  // return damage
  let receivedDamage = getHitPower(attacker) - getBlockPower(defender);
  receivedDamage = receivedDamage > 0 ? receivedDamage : 0;
  return receivedDamage;
}

export function getHitPower(fighter:fighterInFightInt):number {
  // return hit power
  let attack:number = fighter.attack;
  let criticalHitChance = Math.random() + 1;
  let power = attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter:fighterInFightInt):number {
  // return block power
  let defense:number = fighter.defense;
  let dodgeChance = Math.random() + 1;
  let power = defense * dodgeChance;
  return power;
}
