import { createElement } from '../helpers/domHelper';
import { createFightersSelector } from './fighterSelector';
import { fullFighterInt } from '../interfaces/fightersInterfaces'


export function createFighters(fighters:fullFighterInt[]):HTMLElement {
  const selectFighter = createFightersSelector();
  const container:HTMLElement = createElement({ tagName: 'div', className: 'fighters___root' });
  const preview:HTMLElement = createElement({ tagName: 'div', className: 'preview-container___root' });
  const fightersList:HTMLElement = createElement({ tagName: 'div', className: 'fighters___list' });
  const fighterElements = fighters.map((fighter) => createFighter(fighter, selectFighter));

  fightersList.append(...fighterElements);
  container.append(preview, fightersList);

  return container;
}

function createFighter(fighter:fullFighterInt, selectFighter):HTMLElement {
  const fighterElement:HTMLElement = createElement({ tagName: 'div', className: 'fighters___fighter' });
  const imageElement = createImage(fighter);
  const onClick = (event) => selectFighter(event, fighter._id);

  fighterElement.append(imageElement);
  fighterElement.addEventListener('click', onClick, false);

  return fighterElement;
}

function createImage(fighter:fullFighterInt):HTMLElement {

  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement:HTMLElement = createElement({
    tagName: 'img',
    className: 'fighter___fighter-image',
    attributes
  });

  return imgElement;
}