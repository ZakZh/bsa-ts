import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  ///////////////////////////////////////////////////////
  if (fighter) {
    const fighterImage = fighterElement.appendChild(createFighterImage(fighter));

    fighterElement.append(createFighterPreviewInfo(fighter));

    if (position === 'right') {
      fighterImage.style.transform = 'scale(-1, 1)';
    }
  }

  return fighterElement;
}

export function createFighterPreviewInfo(fighter) {
  const { name, health, attack, defense } = fighter;

  const infoElement = createElement({
    tagName: 'div',
    className: 'fighter-preview__info',
  });

  const fighterName = name;
  const fighterNameStyleClass = 'fighter-preview__name';

  const characteristics = {
    health: health,
    attack: attack,
    defense: defense,
  };

  const fighterCharacteristicsStyleClass = 'fighter-preview__characteristic';

  infoElement.append(createCharacteristicElement(fighterName, fighterNameStyleClass));

  for (let [key, value] of Object.entries(characteristics)) {
    const characteristic = `${key} : ${value}`;

    infoElement.append(createCharacteristicElement(characteristic, fighterCharacteristicsStyleClass));
  }

  return infoElement;
}

export function createCharacteristicElement(characteristic, className) {
  const characteristicElement = createElement({
    tagName: 'p',
    className: className,
  });
  characteristicElement.innerText = characteristic;
  return characteristicElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
