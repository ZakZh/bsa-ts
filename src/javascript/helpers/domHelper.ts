type attributesType = {
  [key: string]: any
}

interface htmlElementInt {
  tagName: string;
  className?:string;
  attributes?:attributesType;
}

export function createElement(el:htmlElementInt):HTMLElement {
  const element = document.createElement(el.tagName);


  if (el.className) {
    const classNames = el.className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }
  if (el.attributes !== undefined) {
    Object.keys(el.attributes).forEach((key:string) => {
      element.setAttribute(key, el.attributes[key])}
    );
  }

  return element;
}
