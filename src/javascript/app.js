import { createFighters } from './components/fightersView';
import { fighterService } from './services/fightersService';
class App {
    constructor() {
        this.rootElement = this.htmlNullChecker(document.getElementById('root'));
        this.loadingElement = this.htmlNullChecker(document.getElementById('loading-overlay'));
        this.startApp();
    }
    htmlNullChecker(el) {
        if (el === null) {
            throw new Error("Can't access to element!");
        }
        return el;
    }
    async startApp() {
        try {
            this.loadingElement.style.visibility = 'visible';
            const fighters = await fighterService.getFighters();
            const fightersElement = createFighters(fighters);
            this.rootElement.appendChild(fightersElement);
        }
        catch (error) {
            console.warn(error);
            this.rootElement.innerText = 'Failed to load data';
        }
        finally {
            this.loadingElement.style.visibility = 'hidden';
        }
    }
}
export default App;
