import { callApi } from '../helpers/apiHelper';
class FighterService {
    constructor() {
        this.endpoint = "";
        this.apiResult = Object;
    }
    async getFightersData(id) {
        try {
            this.endpoint = id ? `details/fighter/${id}.json` : 'fighters.json';
            this.apiResult = await callApi(this.endpoint, 'GET');
            return this.apiResult;
        }
        catch (error) {
            throw error;
        }
    }
    async getFighters() {
        return await this.getFightersData();
    }
    async getFighterDetails(id) {
        return await this.getFightersData(id);
    }
}
export const fighterService = new FighterService();
