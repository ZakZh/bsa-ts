import { callApi } from '../helpers/apiHelper';

class FighterService {
  private endpoint:string ="";
  private apiResult:object|object[] = Object;

  private async getFightersData(id?:string) {
    try {
      this.endpoint = id ?`details/fighter/${id}.json`: 'fighters.json';
      this.apiResult = await callApi(this.endpoint, 'GET');
      return this.apiResult;
    } catch (error) {
      throw error;
    }
  }

  public async getFighters() {
    return await this.getFightersData();
  }

  public async getFighterDetails(id:string) {
    return await this.getFightersData(id);
  }
}

export const fighterService = new FighterService();