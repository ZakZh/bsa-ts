export interface shortFightersInt {
  _id:string;
  name: string;
  source: string;
}

export interface fullFighterInt extends shortFightersInt {
  health: number;
  attack: number;
  defense: number;
}

export interface fighterInFightInt extends fullFighterInt {
  blockStatus: boolean,
  healthBar: HTMLElement|null,
  healthOnStart: number,
  fighterCombos: SetConstructor,
  comboStatus: {
    lastCombo: number,
    comboInput: SetConstructor,
  },
}

